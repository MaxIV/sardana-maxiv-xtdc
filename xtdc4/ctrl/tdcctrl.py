from tango import DevState, DeviceProxy, EventType
from sardana.pool.controller import OneDController
from sardana import State, DataAccess
from sardana.pool.controller import Type, Access, Description, MaxDimSize

import time
import numpy

class TdcController(OneDController):
    '''1D controller. It starts an aquisition on a
    time-to-digital converter device
    https://github.com/aduc812/xTDC4-tango
    Then it listens to events containing time stamps, 
    and builds a histogram of these.'''

    ctrl_properties = {
        'TDCDeviceName': {
            'type': str,
            'description': 'TDC device name'
        },
        'TDCAcqTime': {
            'type': str,
            'description': 'Acquisition time attribute'
        },
        'TDCApply': {
            'type': str,
            'description': 'Apply settings command'
        },
        'TDCStart': {
            'type': str,
            'description': 'Start command'
        },
        'TDCStop': {
            'type': str,
            'description': 'Stop command'
        },
        'TDCCounts': {
            'type': str,
            'description': 'Attribute providing the events'
        },
    }

    axis_attributes = {
        "spectrum": {
            Type: [int],
            Description: 'The spectrum while being accumulated, binned for display',
            Access: DataAccess.ReadOnly,
            MaxDimSize: (2**24,)
        },
        "timescale": {
            Type: [float],
            Description: 'The timescale for the current spectrum, binned for display, ns',
            Access: DataAccess.ReadOnly,
            MaxDimSize: (2**24,)
        },
        "timescale_full": {
            Type: [float],
            Description: 'The timescale for the current spectrum, ns',
            Access: DataAccess.ReadOnly,
            MaxDimSize: (2**24,)
        },
        "clockrate": {
            Type: float,
            Description: 'The clock frequency of the xtdc card, Hz',
            Access: DataAccess.ReadWrite
        },
        "xrayrate": {
            Type: float,
            Description: 'The repetition rate of the xray source, Hz',
            Access: DataAccess.ReadWrite
        },
        "delay": {
            Type: float,
            Description: 'Delay before first pulse, ns',
            Access: DataAccess.ReadWrite
        },
        "binning": {
            Type: int,
            Description: 'Number of bins to merge, for display',
            Access: DataAccess.ReadWrite
        },
        "DataLength": {
            Type: int, 
            Access: DataAccess.ReadWrite
        }
    }

    def __init__(self, inst, props):
        OneDController.__init__(self, inst, props)
        self.acqtime = 0
        self.busy = False
        self._counts = []
        self._timescale_full = None
        self._timescale = None
        self._clockrate = 76745970836.0 #13.03 ps/bin
        self._xrayrate = 3120642.9 # ring rep rate
        self._delay = 269.0 # delay until first pulse
        self._binning = 10
        self._event_id = None
        try:
            self.tdc = DeviceProxy(self.TDCDeviceName)
        except Exception as e:
            self._log.error(e)

    def StateOne(self, axis):
        #if self.busy:
        #    return (DevState.MOVING, "Measuring...")
        #else:
        #    return (DevState.ON, "Device is on")
        return (self.tdc.state(), self.tdc.status())

    def PreReadOne(self, axis):
        self._log.debug("PreReadOne")

    def ReadOne(self, axis):
        self._log.debug("ReadOne")
        while self.tdc.state() == DevState.RUNNING:
            time.sleep(0.1)
        if self._event_id:
            self.tdc.unsubscribe_event(self._event_id)
            self._event_id = None
        self.busy=False
        hist = self.make_histogram()
        self._log.debug("ReadOne done")
        return hist

    def LoadOne(self, axis, value):
        "Set acquisition time, units???"
        self._log.debug("LoadOne {} s".format(value))
        self.acqtime = value
        self.tdc.write_attribute(self.TDCAcqTime, value)
        self.tdc.command_inout(self.TDCApply)
        self._counts = []
        self._log.debug("LoadOne done")
        return True

    def StartOne(self, axis, value):
        self._log.debug("StartOne {}".format(value))
        self.setup_subscription()
        self.tdc.command_inout(self.TDCStart)
        self.busy = True
        self._log.debug("StartOne done")

    def StopOne(self, axis):
        self.tdc.command_inout(self.TDCStop)
        self.busy=False
        if self._event_id:
            self.tdc.unsubscribe_event(self._event_id)
            self._event_id = None


    def Abort(self, axis):
        self.tdc.command_inout(self.TDCStop)
        self.busy=False
        if self._event_id:
            self.tdc.unsubscribe_event(self._event_id)
            self._event_id = None

    def GetAxisExtraPar(self, axis, name):
        self._log.debug("get {}".format(name))
        if name.lower() == "clockrate":
            return self._clockrate
        elif name.lower() == "xrayrate":
            return self._xrayrate
        elif name.lower() == "delay":
            return self._delay
        elif name.lower() == "binning":
            return self._binning
        elif name.lower() == "spectrum":
            return self.make_histogram(binning=True)
        elif name.lower() == "timescale":
            return self._timescale
        elif name.lower() == "timescale_full":
            return self._timescale_full
        elif name.lower() == "datalength":
            return self.calc_length()

    def SetAxisExtraPar(self, axis, name, value):
        if name.lower() == "clockrate":
            self._clockrate = value
        elif name.lower() == "xrayrate":
            self._xrayrate = value
        elif name.lower() == "delay":
            self._delay = value
        elif name.lower() == "binning":
            self._binning = value

    def calc_length(self):
        interval = round(self._clockrate / self._xrayrate)
        # TODO extend max length pas 16384 to get rid of factor 2 binning
        length = int(interval/2)
        self._log.debug("length is {}".format(length))
        return length

    def make_histogram(self, binning=False):
        self._log.debug("make histogram")
        if binning:
            binfact = self._binning
        else:
            # TODO extend max length pas 16384 to get rid of factor 2 binning
            binfact = 2
        delay_bins = self._delay/1.0e9 * self._clockrate
        interval = round(self._clockrate / self._xrayrate)
        counts = numpy.array(self._counts)
        allcounts_fold = (counts-delay_bins)%interval
        hist=numpy.histogram(allcounts_fold, bins=int(interval/binfact), range=(0, interval))
        if binning:
            self._timescale = 1.0e9/self._clockrate * 0.5*(hist[1][0:-1:]+hist[1][1::])
        else:
            self._timescale_full = 1.0e9/self._clockrate * 0.5*(hist[1][0:-1:]+hist[1][1::])
        self._log.debug("make histogram done")
        return hist[0]

    def setup_subscription(self):
        self._log.debug("setup subscription")
        self._event_id = self.tdc.subscribe_event(
                self.TDCCounts,
                EventType.CHANGE_EVENT,
                self)
        self._log.debug("subscr done")
    
    def stop_subscription(self):
        self.tdc.unsubscribe_event(self._event_id)


    def push_event(self, event):
        self._log.debug("got event")
        attr = event.attr_value
        if attr:
            if attr.name.lower() == self.TDCCounts.lower():
                counts = attr.value
                if counts is not None:
                    self._log.debug("{} counts".format(len(counts)))
                    self._counts.extend(counts)
                    self._log.debug("{} total counts".format(len(self._counts)))
                
                    

