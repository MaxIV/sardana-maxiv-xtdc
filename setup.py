#!/usr/bin/env python

from setuptools import setup, find_packages
import sys

TESTING = any(x in sys.argv for x in ['pytest'])

setup(
    name="sardana-xtdc",
    version="0.1.1", 
    packages=find_packages(),
    
    tests_require=["setuptools",
                   "pytest"],
    
    install_requires=["setuptools", 
                      "pytango"],

    setup_requires=["pytest-runner"] if TESTING else [],
    description = "A counter for the XTDC4 time-to-digital card",
    author = "KITS - Controls",
    author_email = "KITS@maxiv.lu.se",
    license = "GPLv3",
    url = "https://gitlab.maxiv.lu.se/kits-maxiv/sardana-xtdc",
)