import pytest
import tango
import math


@pytest.fixture()
def controller():
    from xtdc4.ctrl import TdcController

    ctrl = TdcController(
        "test",
        {
            "TDCDeviceName": "dummy1",
            "TDCAcqTime": "dummy2",
            "TDCApply": "dummy3",
            "TDCStart": "dummy4",
            "TDCStop": "dummy5",
            "TDCCounts": "data",
        },
    )
    return ctrl


class dummy_attr_value():
    def __init__(self, name, vals):
        self.name = name
        self.value = vals

class dummy_event():
    def __init__(self, name, vals):
        self.attr_value = dummy_attr_value(name,vals)

def test_attributes(controller):
    controller.SetAxisExtraPar(1, "delay", 1)
    controller.SetAxisExtraPar(1, "binning", 2)
    controller.SetAxisExtraPar(1, "clockrate", 3)
    controller.SetAxisExtraPar(1, "xrayrate", 4)
    assert controller.GetAxisExtraPar(1, "delay") == 1
    assert controller.GetAxisExtraPar(1, "binning") == 2
    assert controller.GetAxisExtraPar(1, "clockrate") == 3
    assert controller.GetAxisExtraPar(1, "xrayrate") == 4

def test_push_event(controller):
    controller.SetAxisExtraPar(1, "delay", 0)
    controller.SetAxisExtraPar(1, "binning", 1)
    controller.SetAxisExtraPar(1, "clockrate", 1000)
    controller.SetAxisExtraPar(1, "xrayrate", 100)
    for n in range(100):
        event = dummy_event("data",[n])
        controller.push_event(event)
    for n in range(0,50,10):
        event = dummy_event("data",[n])
        controller.push_event(event)
    for n in range(1,30,10):
        event = dummy_event("data",[n])
        controller.push_event(event)
    hist = controller.GetAxisExtraPar(1, "spectrum")
    scale = controller.GetAxisExtraPar(1, "timescale")
    assert hist[0] == 15
    assert hist[1] == 13
    for n in range(2,10):
        assert hist[n] == 10
    for n in range(10):
        assert scale[n] == (n+0.5)*1e6

def test_push_event_binning(controller):
    controller.SetAxisExtraPar(1, "delay", 0)
    controller.SetAxisExtraPar(1, "binning", 2)
    controller.SetAxisExtraPar(1, "clockrate", 1000)
    controller.SetAxisExtraPar(1, "xrayrate", 100)
    for n in range(100):
        event = dummy_event("data",[n])
        controller.push_event(event)
    for n in range(0,50,10):
        event = dummy_event("data",[n])
        controller.push_event(event)
    for n in range(1,30,10):
        event = dummy_event("data",[n])
        controller.push_event(event)
    hist = controller.GetAxisExtraPar(1, "spectrum")
    scale = controller.GetAxisExtraPar(1, "timescale")
    assert hist[0] == 28
    for n in range(1,5):
        assert hist[n] == 20
    for n in range(5):
        assert scale[n] == (2*n+1)*1e6
